# Rampify

![](images/v01.png)

A tool that makes it easier to make color ramps from images.

### Interactions

Generally available:

 - `LEFT MOUSE BUTTON`: Selects a vertex the mouse is on top of.
 - `CTRL + S`: Saves both the ramp and the state of the program. Name is according to the input image without its extension and `-ramp.png` or `-state.yaml` accordingly attached.
 - `CTRL + MOVE MOUSE`: Move the input texture around.

When no vertex is selected:

 - `ALT + MOVING MOUSE`: Move the selected vertex.
 - `CTRL + LEFT MOUSE BUTTON`: Allows you to add a vertex to the end of the the graph.
 - `CTRL + ALT + LEFT MOUSE BUTTON`: Allows you to insert a vertex in the graph. The line represents where the vertex how the vertex will be inserted exactly.

When a vertex is selected:

 - `D`: Deletes the selected vertex.

### Command line arguments

 - `h, --help`: display the help message.
 - `i, --image` + string: the texture to use as input.
 - `y, --yaml` + string: the config file to use as input.

If both a config file and a texture is supplied then only the config file is read.