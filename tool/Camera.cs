﻿using System;
using System.Collections.Generic;
using System.Text;

using OpenTK.Mathematics;

namespace Rampify
{
    class Camera
    {
        /// <summary>
        /// Coordinates of the camera.
        /// </summary>
        public Vector2i coordinates;
    }
}
