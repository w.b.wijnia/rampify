﻿using System;
using System.Collections.Generic;
using System.Text;

using OpenTK.Windowing.GraphicsLibraryFramework;

namespace Rampify
{
    public static class Hotkeys
    {
        public enum HotkeyState { 
            CTRL = 4,
            ALT = 8,
            SHIFT = 16,
            Q = 32,
            W = 64,
            E = 128,
            A = 256,
            S = 512,
            D = 1024,
            Z = 2048,
            X = 4096,
            C = 8192,
        }

        private static Dictionary<Keys, int> keys = new Dictionary<Keys, int>();
        private static Dictionary<MouseButton, int> buttons = new Dictionary<MouseButton, int>();

        /// <summary>
        /// Represents the state of the keyboard.
        /// </summary>
        private static int _state;

        public static int State
        {
            get { return _state; }
        }

        static Hotkeys()
        {
            keys.Add(Keys.LeftControl, (int)HotkeyState.CTRL);
            keys.Add(Keys.LeftAlt, (int)HotkeyState.ALT);
            keys.Add(Keys.LeftShift, (int)HotkeyState.SHIFT);
            keys.Add(Keys.Q, (int)HotkeyState.Q);
            keys.Add(Keys.W, (int)HotkeyState.W);
            keys.Add(Keys.E, (int)HotkeyState.E);
            keys.Add(Keys.A, (int)HotkeyState.A);
            keys.Add(Keys.S, (int)HotkeyState.S);
            keys.Add(Keys.D, (int)HotkeyState.D);
            keys.Add(Keys.Z, (int)HotkeyState.Z);
            keys.Add(Keys.X, (int)HotkeyState.X);
            keys.Add(Keys.C, (int)HotkeyState.C);
        }

        public static void Update(KeyboardState keyboard)
        {
            _state = 0;

            foreach (var kv in keys)
                if (keyboard.IsKeyDown(kv.Key))
                    _state |= kv.Value;
        }

        public static bool IsUp(params Keys[] input)
        {
            bool allUp = true;
            foreach (Keys key in input)
            {
                if (!keys.ContainsKey(key))
                    Console.WriteLine($"Unknown key: {key}");

                allUp &= ((_state & keys[key]) == 0);
            }

            return allUp;
        }

        public static bool IsDown(params Keys[] input)
        {
            int state = 0;
            foreach (Keys key in input)
            {
                if (!keys.ContainsKey(key))
                    Console.WriteLine($"Unknown key: {key}");

                state |= (int)keys[key];
            }

            return _state == state;
        }
    }
}
