﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rampify
{
    public static class Kernels
    {

        private static Dictionary<int, float[]> kernels = new Dictionary<int, float[]>();

        public static ReadOnlySpan<float> GaussianBlur(int size, float weight)
        {
            // check if we've already made this kernel before
            if (kernels.ContainsKey(size))
            {
                // return previous made version
                return new ReadOnlySpan<float>(kernels[size]); ;
            }

            float[] kernel = new float[size * size];
            float kernelSum = 0;
            int foff = (size - 1) / 2;
            float distance = 0;
            float constant = 1.0f / (2.0f * (float)Math.PI * weight * weight);

            // construct it
            for (int y = -foff; y <= foff; y++)
            {
                for (int x = -foff; x <= foff; x++)
                {
                    distance = ((y * y) + (x * x)) / (2 * weight * weight);
                    kernel[(x + foff) + (y + foff) * size] = constant * (float)Math.Exp(-distance);
                    kernelSum += kernel[(x + foff) + (y + foff) * size];
                }
            }

            // normalize it
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    kernel[x + y * size] = kernel[x + y * size] * 1.0f / kernelSum;
                }
            }

            // store it
            kernels[size] = kernel;

            // return it
            return new ReadOnlySpan<float>(kernels[size]);
        }
    }
}
