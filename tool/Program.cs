﻿
using OpenTK.Mathematics;
using OpenTK.Windowing.Desktop;

using System;
using System.IO;
using System.Collections.Generic;

using CommandLine;
using CommandLine.Text;

namespace Rampify
{

    class Options
    {
        [Option('h', "help", Required = false, HelpText = "Shows the help message of how to use the CLI of this application.")]
        public bool Help { get; set; }

        [Option('i', "image", Required = false, HelpText = "Image to read color data from.")]
        public string Albedomap { get; set; }

        [Option('m', "heightmap", Required = false, HelpText = "Image to read heightmap data from.")]
        public string Heightmap { get; set; }

        [Option('y', "yaml", Required = false, HelpText = "Configuration to read.")]
        public string Config { get; set; }
    }
    class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Not enough arguments.");
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
                return;
            }

            Parser.Default.ParseArguments<Options>(args)
              .WithParsed(RunOptions)
              .WithNotParsed(HandleParseError);
        }

        static void RunOptions(Options opts)
        {
            // either of these is mandatory for now
            if (string.IsNullOrEmpty(opts.Albedomap) && string.IsNullOrEmpty(opts.Config))
            {
                Console.WriteLine("No albedo map or configuration provided. ");
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
                return;
            }

            // proceed with configuration file
            if (!string.IsNullOrEmpty(opts.Config))
            {
                if (!string.IsNullOrEmpty(opts.Albedomap))
                    Console.WriteLine("Configuration and albedo map provided: using the configuration to determine the albedo map.");

                string config = opts.Config;

                if (!File.Exists(opts.Config))
                {
                    Console.Error.WriteLine($"Config file does not exist: {opts.Config}");
                    Console.WriteLine("Press any key to exit...");
                    Console.ReadKey();
                    return;
                }

                State state = State.LoadFromConfig(config);
                StartApplication();
                return;
            }

            // proceed with color image and no configuration file
            if (!string.IsNullOrEmpty(opts.Albedomap))
            {
                if (!File.Exists(opts.Albedomap))
                {
                    Console.Error.WriteLine($"Albedo file does not exist: {opts.Config}");
                    Console.WriteLine("Press any key to exit...");
                    Console.ReadKey();
                    return;
                }

                State state = State.LoadFromAlbedo(opts.Albedomap);
                StartApplication();
                return;
            }
        }

        static void StartApplication()
        {
            GameWindowSettings gameWindowSettings = new GameWindowSettings();
            gameWindowSettings.IsMultiThreaded = false;
            gameWindowSettings.RenderFrequency = 60.0;
            gameWindowSettings.UpdateFrequency = 60.0;

            NativeWindowSettings nativeWindowsettings = new NativeWindowSettings()
            {
                Size = new Vector2i(1044, 600),
                Title = "Rampify",
            };

            using (Window app = new Window(gameWindowSettings, nativeWindowsettings))
            {
                app.Run();
            }
        }

        static void HandleParseError(IEnumerable<Error> errs)
        {
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}