﻿using System;

using OpenTK.Graphics.OpenGL;

namespace Rampify
{
    class Quad
    {
		// the vertex data of the quad
		private readonly float[] _vertices =
		{
            // Position         Texture coordinates
             1.0f,  1.0f, 0.0f, 1.0f, 0.0f, // top right
             1.0f, -1.0f, 0.0f, 1.0f, 1.0f, // bottom right
            -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, // bottom left
            -1.0f,  1.0f, 0.0f, 0.0f, 0.0f  // top left
        };

		// the indices of the quad
		private readonly uint[] _indices =
		{
			0, 1, 3,
			1, 2, 3
		};

		// opengl handles
		private int _elementBufferObject;
		private int _vertexBufferObject;
		private int _vertexArrayObject;

		// a shader class taken from OpenTK
		private Shader _shader;

		private int _screenHandle;
		public Surface screen;

		public Quad(Surface screen)
		{ this.screen = screen; }

		public void OnLoad()
		{
			// prepare the vertex structures
			_vertexArrayObject = GL.GenVertexArray();
			GL.BindVertexArray(_vertexArrayObject);

			_vertexBufferObject = GL.GenBuffer();
			GL.BindBuffer(BufferTarget.ArrayBuffer, _vertexBufferObject);
			GL.BufferData(BufferTarget.ArrayBuffer, _vertices.Length * sizeof(float), _vertices, BufferUsageHint.StaticDraw);

			_elementBufferObject = GL.GenBuffer();
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, _elementBufferObject);
			GL.BufferData(BufferTarget.ElementArrayBuffer, _indices.Length * sizeof(uint), _indices, BufferUsageHint.StaticDraw);

			// prepare the shaders
			_shader = new Shader("assets/shaders/shader.vert", "assets/shaders/shader.frag");
			_shader.Use();

			// position data
			var vertexLocation = _shader.GetAttribLocation("aPosition");
			GL.EnableVertexAttribArray(vertexLocation);
			GL.VertexAttribPointer(vertexLocation, 3, VertexAttribPointerType.Float, false, 5 * sizeof(float), 0);

			// texture data
			var texCoordLocation = _shader.GetAttribLocation("aTexCoord");
			GL.EnableVertexAttribArray(texCoordLocation);
			GL.VertexAttribPointer(texCoordLocation, 2, VertexAttribPointerType.Float, false, 5 * sizeof(float), 3 * sizeof(float));

			// initialise the texture
			_screenHandle = screen.GenTexture();
			GL.ActiveTexture(TextureUnit.Texture0);
			GL.BindTexture(TextureTarget.Texture2D, _screenHandle);
		}

		public void OnUnload()
		{
			// Delete all the resources.
			GL.DeleteBuffer(_vertexBufferObject);
			GL.DeleteVertexArray(_vertexArrayObject);
			GL.DeleteProgram(_shader.Handle);
		}

		public void OnRender()
		{
			// bind the texture, then populate it
			GL.BindTexture(TextureTarget.Texture2D, _screenHandle);

			// populate the texture
			GL.TexImage2D(TextureTarget.Texture2D,
				0,
				PixelInternalFormat.Rgba,
				screen.width,
				screen.height,
				0,
				PixelFormat.Bgra,
				PixelType.Float,
				screen.pixelsFloat);

			// bind the quad, shader and activate texture
			GL.BindVertexArray(_vertexArrayObject);
			GL.ActiveTexture(TextureUnit.Texture0);
			_shader.Use();

			// draw it all out
			GL.DrawElements(PrimitiveType.Triangles, _indices.Length, DrawElementsType.UnsignedInt, 0);
		}
	}
}
