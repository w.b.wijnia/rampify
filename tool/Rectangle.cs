﻿
using OpenTK.Mathematics;
namespace Rampify
{
    class RectangleAxis
    {

        public Vector2i origin, dimensions;

        public RectangleAxis(Vector2i origin, Vector2i dimensions)
        {
            this.origin = origin;
            this.dimensions = dimensions;
        }

        public bool CheckOverlap(RectangleAxis other)
        {
            throw new System.Exception("Not implemented");
        }

        public RectangleAxis Overlap(RectangleAxis other)
        {
            throw new System.Exception("Not implemented");
        }
    }
}
