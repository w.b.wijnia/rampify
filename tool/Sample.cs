﻿using System;
using System.Collections.Generic;
using System.Text;

using OpenTK.Mathematics;

namespace Rampify
{
    class Sample
    {

        /// <summary>
        /// Location of this sample on the image.
        /// </summary>
        public Vector2i PositionOnTexture;

        /// <summary>
        /// Location of this sample on the ramp.
        /// </summary>
        public int IndexOnRamp;

        /// <summary>
        /// Kernel size of this sample.
        /// </summary>
        public int Size;

        /// <summary>
        /// Color of this sample.
        /// </summary>
        public Vector4 Color;

        /// <summary>
        /// The weight of this sample
        /// </summary>
        public float Weight;

        public bool Skip;

    }
}
