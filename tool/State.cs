﻿using System;
using System.Collections.Generic;
using System.Text;

using OpenTK.Mathematics;

using System.IO;

using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;
using YamlDotNet.Helpers;
using YamlDotNet.Core;
using YamlDotNet.Serialization.ObjectGraphTraversalStrategies;

namespace Rampify
{
    class State
    {

        #region Mouse information

        /// <summary>
        /// The last position of the mouse.
        /// </summary>
        [YamlIgnore]
        public Vector2i MousePosition;

        /// <summary>
        /// Whether the mouse has moved during clicking.
        /// </summary>
        [YamlIgnore]
        public bool MouseMoved;

        #endregion

        #region Session information

        /// <summary>
        /// Nearest vertex with respect to the mouse position. Updated each frame.
        /// </summary>
        [YamlIgnore]
        public int NearestVertex;

        /// <summary>
        /// Nearest segment with respect to the mouse position. Updated each frame.
        /// </summary>
        [YamlIgnore]
        public int NearestSegment;

        /// <summary>
        /// Nearest point of the graph with respect to the mouse position. Updated each frame.
        /// </summary>
        [YamlIgnore]
        public Vector2i NearestPoint;

        /// <summary>
        /// The identifier of the selected vertex.
        /// </summary>
        [YamlIgnore]
        public int SelectedVertex;

        [YamlIgnore]
        public List<Sample> Samples = new List<Sample>();

        /// <summary>
        /// Origin of the loaded texture.
        /// </summary>
        [YamlIgnore]
        public Vector2i TextureOrigin;

        /// <summary>
        /// Texture to generate a ramp from.
        /// </summary>
        [YamlIgnore]
        public Surface Texture;

        /// <summary>
        /// The ramp generated from the texture.
        /// </summary>
        [YamlIgnore]
        public Surface Ramp;

        #endregion

        #region Program information

        /// <summary>
        /// Height of the ramp for easier visualisation.
        /// </summary>
        [YamlIgnore]
        public int RampHeight = 15;

        /// <summary>
        /// Offset for the ramp to ensure it is below the main menu bar.
        /// </summary>
        [YamlIgnore]
        public Vector2i RampOffset = new Vector2i(10, 25);

        #endregion

        #region Program state

        /// <summary>
        /// The input texture to read from.
        /// </summary>
        [YamlIgnore]
        public string InputPath { get; set; }

        /// <summary>
        /// List of vertices to make the ramp with.
        /// </summary>
        [YamlIgnore]
        public List<Vertex> Vertices { get; set; }

        #endregion

        /// <summary>
        /// The program state.
        /// </summary>
        [YamlIgnore]
        private static State _state;

        FileSystemWatcher watcher = new FileSystemWatcher();

        /// <summary>
        /// Constructs an empty state.
        /// </summary>
        private State() 
        {
            this.SelectedVertex = -1;
            this.Vertices = new List<Vertex>();
        }

        ~State()
        {
            watcher.Dispose();
        }

        /// <summary>
        /// Reconstructs the state from a serializable version of the state.
        /// </summary>
        /// <param name="other"></param>
        private State(StateSerializable other)
        {
            this.SelectedVertex = -1;
            this.InputPath = other.Source;

            // convert them back to vertices
            this.Vertices = new List<Vertex>();
            foreach (VertexSerializable vs in other.Vertices)
                this.Vertices.Add(new Vertex(vs));
        }

        /// <summary>
        /// Finds the nearest vertex to the given position. Assumes that there is at least one vertex.
        /// </summary>
        public int FindNearestVertex(Vector2i position)
        {
            int id = 0;
            float idDistance = float.MaxValue;

            for (int j = 0; j < Vertices.Count; j++)
            {
                Vertex v = Vertices[j];

                float distance = (v.coordinates - position).EuclideanLength;
                if (distance < idDistance)
                {
                    id = j;
                    idDistance = distance;
                }
            }

            return id;
        }

        /// <summary>
        /// Finds the nearest point when taking into account the line segments between vertices. Assumes there is at least one vertex.
        /// </summary>
        public int FindNearestPoint(Vector2i position, ref Vector2i closest, float epsilon = 1.0f)
        {
            // default values
            int id = 0;
            float idDistance = float.MaxValue;

            // find the real value
            for (int j = 0; j < Vertices.Count - 1; j++)
            {
                Vertex v1 = Vertices[j];
                Vertex v2 = Vertices[j + 1];

                Vector2i point;
                float distance = Utilities.DistanceToLine(v1.coordinates, v2.coordinates, MousePosition - TextureOrigin, out point);
                if (distance < idDistance)
                {
                    id = j;
                    idDistance = distance;
                    closest = point;
                }
            }

            return id;
        }

        /// <summary>
        /// Computes the total length of the graph of vertices.
        /// </summary>
        /// <returns></returns>
        public float TotalLengthOfGraph()
        {
            float length = 0.0f;

            // find the real value
            for (int j = 0; j < Vertices.Count - 1; j++)
            {
                Vertex v1 = Vertices[j];
                Vertex v2 = Vertices[j + 1];

                if (! (v1.skip && v2.skip))
                    length += (v1.coordinates - v2.coordinates).EuclideanLength;
            }

            return length;
        }

        /// <summary>
        /// Loads the program state from a file.
        /// </summary>
        public static State LoadFromConfig(string file)
        {
            if(!File.Exists(file))
                throw new Exception($"File does not exist: { file }");

            var deserializer = new DeserializerBuilder()
                .Build();

            using (StreamReader reader = new StreamReader(file))
            {
                string content = reader.ReadToEnd();
                Console.WriteLine(content);
                _state = new State(deserializer.Deserialize<StateSerializable>(content));
            }

            // initialize the state
            if (!File.Exists(_state.InputPath))
                Console.Error.WriteLine("Error: cannot find albedo input file.");

            _state.Texture = new Surface(_state.InputPath);
            _state.Ramp = new Surface(1024, 1);
            _state.Samples = new List<Sample>();

            Console.WriteLine(file);
            Console.WriteLine(Path.GetFullPath(Path.GetDirectoryName(file)));

            _state.watcher.Path = Path.GetFullPath(Path.GetDirectoryName(file));
            _state.watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite; 
            _state.watcher.Filter = Path.GetFileName(_state.InputPath);
            _state.watcher.Changed += new FileSystemEventHandler(OnChanged);
            _state.watcher.EnableRaisingEvents = true;

            return _state;
        }

        public static void OnChanged(object source, FileSystemEventArgs e)
        {
            // Specify what is done when a file is changed.  
            Console.WriteLine("Input texture updated through an external source.");

            try { _state.Texture = new Surface(e.FullPath); }
            catch (Exception exception)
            {
                Console.WriteLine("Was unable to update input texture. Try again by saving the texture again in a bit.");
                Console.Error.WriteLine(exception);
            }
        }

        public static State LoadFromAlbedo(string file)
        {
            if (!File.Exists(file))
                throw new Exception($"File does not exist: { file }");

            _state = new State();
            _state.InputPath = file;
            _state.Texture = new Surface(_state.InputPath);
            _state.Ramp = new Surface(1024, 1);
            _state.Samples = new List<Sample>();

            _state.watcher.Path = Path.GetFullPath(Path.GetDirectoryName(file));
            _state.watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite;
            _state.watcher.Filter = Path.GetFileName(_state.InputPath);
            _state.watcher.Changed += new FileSystemEventHandler(OnChanged);
            _state.watcher.EnableRaisingEvents = true;

            return _state;
        }

        /// <summary>
        /// Saves the program state to a file.
        /// </summary>
        public static void SaveToFile()
        {

            string path = _state.InputPath.Split(".")[0];
            string pathState = path + "-state.yaml";
            string pathRamp = path + "-ramp.png";


            // serialize it
            var serializer = new SerializerBuilder().Build();
            var yaml = serializer.Serialize(new StateSerializable(_state));

            // write out the state
            using (StreamWriter writer = new StreamWriter(pathState))
                writer.WriteLine(yaml);

            // write out the ramp 

            _state.Ramp.Save(pathRamp);
        }

        /// <summary>
        /// Retrieves the program state.
        /// </summary>
        /// <returns></returns>
        public static State GetState()
        {
            if (_state == null)
                _state = new State();

            return _state;
        }
    }
}
