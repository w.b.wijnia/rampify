﻿using System;
using System.Collections.Generic;
using System.Text;

using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace Rampify
{

    class StateSerializable
    {
        /// <summary>
        /// The input texture to read from.
        /// </summary>
        [YamlMember]
        public string Source { get; set; }

        /// <summary>
        /// List of vertices to make the ramp with.
        /// </summary>
        [YamlMember]
        public List<VertexSerializable> Vertices { get; set; }

        public StateSerializable()
        {
            this.Source = "";
            this.Vertices = new List<VertexSerializable>();
        }

        public StateSerializable(State other)
        {
            this.Source = other.InputPath;
            this.Vertices = new List<VertexSerializable>();

            // convert them to serializable vertices
            foreach (Vertex v in other.Vertices)
                this.Vertices.Add(new VertexSerializable(v));
        }

    }
}
