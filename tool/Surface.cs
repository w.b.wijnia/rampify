﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;

namespace Rampify
{
    public class Surface
    {
        public int width, height;
        //public int[] pixels;
        static bool fontReady = false;
        static Surface font;
        static int[] fontRedir;

        public Vector4[] pixelsFloat;

        public Surface(int w, int h)
        {
            width = w;
            height = h;
            pixelsFloat = new Vector4[w * h];
        }
        public Surface(string fileName)
        {
            using (Bitmap bmp = new Bitmap(fileName)) 
            {
                width = bmp.Width;
                height = bmp.Height;
                BitmapData data = bmp.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                IntPtr ptr = data.Scan0;

                // copy over the pixels
                int[] pixels = new int[width * height];
                System.Runtime.InteropServices.Marshal.Copy(data.Scan0, pixels, 0, width * height);
                bmp.UnlockBits(data);

                // convert it to floating point numbers
                pixelsFloat = new Vector4[width * height];
                for (int j = 0; j < pixels.Length; j++)
                    pixelsFloat[j] = ColorToVector(pixels[j]);
            }
        }

        public void Save(string filename)
        {
            int[] pixels = new int[width * height];

            for (int j = 0; j < pixels.Length; j++)
                pixels[j] = VectorToColor(pixelsFloat[j]);

            using (Bitmap bmp = new Bitmap(width, height))
            {
                BitmapData data = bmp.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                IntPtr ptr = data.Scan0;
                Marshal.Copy(pixels, 0, ptr, width * height);
                bmp.UnlockBits(data);
                bmp.Save(filename);
            }
        }

        public int GenTexture()
        {
            int id = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, id);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.Float, pixelsFloat);
            return id;
        }
        public void Clear(Vector4 c)
        {
            for (int s = width * height, p = 0; p < s; p++) pixelsFloat[p] = c;
        }
        public void Box(int x1, int y1, int x2, int y2, Vector4 c)
        {
            int dest = y1 * width;
            for (int y = y1; y <= y2; y++, dest += width)
            {
                pixelsFloat[dest + x1] = c;
                pixelsFloat[dest + x2] = c;

            }

            int dest1 = y1 * width;
            int dest2 = y2 * width;
            for (int x = x1; x <= x2; x++)
            {
                pixelsFloat[dest1 + x] = c;
                pixelsFloat[dest2 + x] = c;

            }
        }
        public void Bar(int x1, int y1, int x2, int y2, Vector4 c)
        {
            int dest = y1 * width;
            for (int y = y1; y <= y2; y++, dest += width) for (int x = x1; x <= x2; x++)
                {
                    pixelsFloat[dest + x] = c;
                }
        }
        public void Line(int x1, int y1, int x2, int y2, Vector4 c)
        {

            if (x1 == x2 && y1 == y2)
                return;

            if ((x1 < 0) || (y1 < 0) || (x2 < 0) || (y2 < 0) ||
                (x1 >= width) || (x2 >= width) || (y1 >= height) || (y2 >= height)) return;
            if (Math.Abs(x2 - x1) > Math.Abs(y2 - y1))
            {
                if (x2 < x1) { int h = x1; x1 = x2; x2 = h; h = y2; y2 = y1; y1 = h; }
                int l = x2 - x1;
                int dy = ((y2 - y1) * 8192) / l;
                y1 *= 8192;
                for (int i = 0; i < l; i++)
                {
                    pixelsFloat[x1++ + (y1 / 8192) * width] = c;
                    y1 += dy;
                }
            }
            else
            {
                if (y2 < y1) { int h = x1; x1 = x2; x2 = h; h = y2; y2 = y1; y1 = h; }
                int l = y2 - y1;
                int dx = ((x2 - x1) * 8192) / l;
                x1 *= 8192;
                for (int i = 0; i < l; i++)
                {
                    pixelsFloat[x1 / 8192 + y1++ * width] = c;
                    x1 += dx;
                }
            }
        }
        public void Plot(int x, int y, Vector4 c)
        {
            if ((x >= 0) && (y >= 0) && (x < width) && (y < height))
            {
                pixelsFloat[x + y * width] = c;
            }
        }
        public void Print(string t, int x, int y, Vector4 c)
        {
            if (!fontReady)
            {
                font = new Surface("assets/font.png");
                string ch = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_-+={}[];:<>,.?/\\ ";
                fontRedir = new int[256];
                for (int i = 0; i < 256; i++) fontRedir[i] = 0;
                for (int i = 0; i < ch.Length; i++)
                {
                    int l = (int)ch[i];
                    fontRedir[l & 255] = i;
                }
                fontReady = true;
            }
            for (int i = 0; i < t.Length; i++)
            {
                int f = fontRedir[(int)t[i] & 255];
                int dest = x + i * 12 + y * width;
                int src = f * 12;
                for (int v = 0; v < font.height; v++, src += font.width, dest += width) for (int u = 0; u < 12; u++)
                    {
                        if ((font.pixelsFloat[src + u].X > 0.1f)) pixelsFloat[dest + u] = c;
                    }
            }
        }

        private Vector4 ColorToVector(int pixel)
        {
            float r = (255 & (pixel >> 0)) / 255.0f;
            float g = (255 & (pixel >> 8)) / 255.0f;
            float b = (255 & (pixel >> 16)) / 255.0f;
            float a = 1.0f;

            return new Vector4(r, g, b, a);
        }

        private int VectorToColor(Vector4 pixel)
        {
            int r = Math.Clamp((int)(255.0f * pixel.X), 0, 255);
            int g = Math.Clamp((int)(255.0f * pixel.X), 0, 255);
            int b = Math.Clamp((int)(255.0f * pixel.X), 0, 255);
            int a = Math.Clamp((int)(255.0f * pixel.X), 0, 255);
            return (a << 24) + (b << 16) + (g << 8) + (r << 0); 
        }

        public Vector4 Sample(Vector2 position)
        {
            Vector2i altPosition = new Vector2i((int)position.X, (int)position.Y);
            return Sample(altPosition);
        }

        public Vector4 Sample(Vector2i position)
        {
            return pixelsFloat[position.X + position.Y * width];
        }

        public Vector4 Sample(Vector2 position, int size, float sigma)
        {
            Vector2i altPosition = new Vector2i((int)position.X, (int)position.Y);
            return Sample(altPosition, size, sigma);
        }

        public Vector4 Sample(Vector2i position, int size, float sigma)
        {
            float sum = 0;
            Vector4 color = Vector4.Zero;

            int hsize = size >> 1;
            ReadOnlySpan<float> kernel = Kernels.GaussianBlur(size, sigma);
            for(int y = 0; y < size; y ++)
            {
                int ly = y - hsize + position.Y;
                if (ly >= 0 && ly < height)
                {
                    for (int x = 0; x < size; x++)
                    {
                        int lx = x - hsize + position.X;
                        if (lx >= 0 && lx < width)
                        {
                            sum += kernel[x + y * size];
                            color += this.pixelsFloat[lx + ly * width];
                        }
                    }
                }
            }

            return (1.0f / (size * size)) * color;
        }
    }
}
