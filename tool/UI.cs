﻿using System;

using OpenTK.Graphics.OpenGL;
using System.Numerics;
using OpenTK.Mathematics;

using OpenTK.Windowing.Desktop;
using OpenTK.Windowing.Common;

using OpenTK.Windowing.GraphicsLibraryFramework;

using Rampify.ImGui;
using ImGuiNET;

namespace Rampify
{
    internal class UI
    {

        // imgui controller similar to C / C++
        ImGuiController _controller;

        /// <summary>
        /// Constructs the UI.
        /// </summary>
        /// <param name="size"></param>
        public UI(Vector2i size)
        {
            _controller = new ImGuiController(size.X, size.Y);
        }

        public bool IsHovered()
        {
            return ImGuiNET.ImGui.IsWindowHovered(ImGuiHoveredFlags.AnyWindow) || ImGuiNET.ImGui.IsAnyItemHovered();
        }

        /// <summary>
        /// Called when the OpenTK window is resized.
        /// </summary>
        public void OnResize(ResizeEventArgs e, Vector2i size)
        {
            _controller.WindowResized(size.X, size.Y);
        }

        /// <summary>
        /// Called each tick to construct the user interface.
        /// </summary>
        public void OnUpdateFrame(GameWindow wnd, FrameEventArgs e)
        {
            // update imgui before we use its values
            _controller.Update(wnd, (float)e.Time);

            MainWindow();
            FloatingWindow();

        }

        private void MainWindow()
        {

            State State = State.GetState();

            if (ImGuiNET.ImGui.BeginMainMenuBar())
            {
                if (ImGuiNET.ImGui.BeginMenu("File"))
                {
                    if (ImGuiNET.ImGui.MenuItem("Open texture", "CTRL + F"))
                    {
                        // todo: open file dialog
                    }

                    if (ImGuiNET.ImGui.MenuItem("Open config", "CTRL + C"))
                    {
                        // todo: open file dialog
                    }

                    if (ImGuiNET.ImGui.MenuItem("Save to disk", "CTRL + S"))
                    {
                        State.SaveToFile();
                    }

                    ImGuiNET.ImGui.EndMenu();
                }

                ImGuiNET.ImGui.EndMainMenuBar();
            }
        }

        private void FloatingWindow()
        {
            ImGuiNET.ImGui.Begin("Properties");
            ImGuiNET.ImGui.SetWindowSize(new System.Numerics.Vector2(400, 400));

            State State = State.GetState();

            if (State.SelectedVertex >= 0)
            {
                Vertex v = State.Vertices[State.SelectedVertex];
                ImGuiNET.ImGui.Text("Properties of vertex");
                ImGuiNET.ImGui.InputInt("X coordinate", ref v.coordinates.X);
                ImGuiNET.ImGui.InputInt("Y coordinate", ref v.coordinates.Y);
                ImGuiNET.ImGui.DragInt("Kernel size", ref v.size, 1.0f, 3, 99);

                // image format is BGRA
                System.Numerics.Vector4 color = new System.Numerics.Vector4(v.color.Z, v.color.Y, v.color.X, v.color.W);
                ImGuiNET.ImGui.ColorEdit4("Color", ref color);
            }

            ImGuiNET.ImGui.End();
        }

        /// <summary>
        /// Called each tick to render the user interface.
        /// </summary>
        /// <param name="e"></param>
        public void OnRenderFrame(FrameEventArgs e)
        {
            _controller.Render();
        }

        /// <summary>
        /// Called when a text input is made.
        /// </summary>
        public void OnTextInput(TextInputEventArgs e)
        {
            // make imgui aware of the input
            _controller.PressChar((char)e.Unicode);
        }

        /// <summary>
        /// Called when a mouse wheel is scrolling.
        /// </summary>
        public void OnMouseWheel(MouseWheelEventArgs e)
        {
            // make imgui aware of the input
            _controller.MouseScroll(e.Offset);
        }
    }
}