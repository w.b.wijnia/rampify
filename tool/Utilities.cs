﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Mathematics;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL4;

namespace Rampify
{
    class Utilities
    {

        public const float Epsilon = 0.001f;

        /// <summary>
        /// Compares the equality between two floats with a minor error-range.
        /// </summary>
        public static bool FloatComparison(float f1, float f2)
        {
            float ab = Math.Abs(f1 - f2);
            return ab < Epsilon;
        }

        /// <summary>
        /// Computes the distance between the two points.
        /// </summary>
        /// <param name="p"></param>
        /// <param name="q"></param>
        /// <returns></returns>
        public static float DistanceToPoint(Vector2i p, Vector2i q)
        {
            return (p - q).EuclideanLength;
        }
        
        /// <summary>
        /// Computes the distance between the two line endpoints and a point.
        /// https://codereview.stackexchange.com/questions/143424/c-code-to-find-distance-between-line-and-point.
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="q"></param>
        /// <returns></returns>
        public static float DistanceToLine(Vector2i p1, Vector2i p2, Vector2i pt, out Vector2i closest)
        {
            float dx = p2.X - p1.X;
            float dy = p2.Y - p1.Y;
            if ((dx == 0) && (dy == 0))
            {
                // It's a point not a line segment.
                dx = pt.X - p1.X;
                dy = pt.Y - p1.Y;

                closest = p1;
                return (float)Math.Sqrt(dx * dx + dy * dy);
            }

            // Calculate the t that minimizes the distance.
            float t = ((pt.X - p1.X) * dx + (pt.Y - p1.Y) * dy) /
                (dx * dx + dy * dy);

            // See if this represents one of the segment's
            // end points or a point in the middle.
            if (t < 0)
            {
                dx = pt.X - p1.X;
                dy = pt.Y - p1.Y;

                closest = new Vector2i(p1.X, p1.Y);
            }
            else if (t > 1)
            {
                dx = pt.X - p2.X;
                dy = pt.Y - p2.Y;

                closest = new Vector2i(p2.X, p2.Y);
            }
            else
            {
                closest = new Vector2i((int)(p1.X + t * dx), (int)(p1.Y + t * dy));

                dx = pt.X - closest.X;
                dy = pt.Y - closest.Y;
            }

            return (float)Math.Sqrt(dx * dx + dy * dy);
        }
    }
}
