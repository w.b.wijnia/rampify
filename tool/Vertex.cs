﻿using System;
using System.Collections.Generic;
using System.Text;

using OpenTK.Mathematics;

namespace Rampify
{
    class Vertex
    {
        /// <summary>
        /// The location on the image.
        /// </summary>
        public Vector2i coordinates;

        /// <summary>
        /// The kernel size used for blurring.
        /// </summary>
        public int size;

        /// <summary>
        /// The color this vertex represents.
        /// </summary>
        public Vector4 color;

        /// <summary>
        /// Whether or not this vertex and segment should be skipped in the color gradient.
        /// </summary>
        public bool skip;

        /// <summary>
        /// A basic vertex.
        /// </summary>
        public Vertex(int x, int y, int size)
        {
            this.coordinates = new Vector2i(x, y);
            this.size = size;
        }

        public Vertex(VertexSerializable v)
        {
            this.coordinates = new Vector2i(v.Cx, v.Cy);
            this.size = v.Size;
            this.skip = v.Skip;

        }

        /// <summary>
        /// Computes the color of this vertex.
        /// </summary>
        /// <param name="screen"></param>
        /// <param name="sigma"></param>
        public void ComputeColor(Surface screen, float sigma)
        {
            this.color = screen.Sample(this.coordinates, this.size, sigma);
        }

        /// <summary>
        /// Renders the vertex on the surface.
        /// </summary>
        public void OnRender(Surface screen, Vector2i offset, Vector4 color)
        {
            int x = coordinates.X;
            int y = coordinates.Y;

            float multiplier = 1.0f;
            if (skip)
            {
                multiplier = 0.25f;
                screen.Box(
                    x + offset.X - ((size + 2) >> 1),
                    y + offset.Y - ((size + 2) >> 1),
                    x + offset.X + ((size + 2) >> 1),
                    y + offset.Y + ((size + 2) >> 1),
                    multiplier * color
                    );
            }

            screen.Box(
                x + offset.X - (size >> 1),
                y + offset.Y - (size >> 1),
                x + offset.X + (size >> 1),
                y + offset.Y + (size >> 1),
                color
                );


        }
    }
}
