﻿using System;
using System.Collections.Generic;
using System.Text;

using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace Rampify
{
    class VertexSerializable
    {
        /// <summary>
        /// Coordinates in image space.
        /// </summary>
        [YamlMember]
        public int Cx, Cy;

        /// <summary>
        /// The kernel size used for blurring.
        /// </summary>
        [YamlMember]
        public int Size;

        /// <summary>
        /// Whether or not this vertex and segment should be skipped in the color gradient.
        /// </summary>
        [YamlMember]
        public bool Skip;

        public VertexSerializable()
        {
            this.Cx = 0;
            this.Cy = 0;
            this.Size = 1;
            this.Skip = false;
        }

        public VertexSerializable(Vertex v)
        {
            this.Cx = v.coordinates.X;
            this.Cy = v.coordinates.Y;

            this.Size = v.size;
            this.Skip = v.skip;
        }
    }
}
