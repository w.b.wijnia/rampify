﻿using System;
using System.Collections.Generic;

using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;

using OpenTK.Windowing.Desktop;
using OpenTK.Windowing.Common;

using OpenTK.Windowing.GraphicsLibraryFramework;

using Rampify.ImGui;

namespace Rampify
{
    public partial class Window : GameWindow
    {

        /// <summary>
        /// A quad that spans the entire screen: allowing us to draw on the screen.
        /// </summary>
        Quad _quad;

        /// <summary>
        /// The UI of this application
        /// </summary>
        UI _ui;

        public Window(GameWindowSettings gameWindowSettings, NativeWindowSettings nativeWindowSettings) : base(gameWindowSettings, nativeWindowSettings)
        {
            State State = State.GetState();
            //State.InputPath = "assets/test-2.jpg";
            //State.Texture = new Surface(State.InputPath);
            State.MousePosition = new Vector2i(0, 0);
            State.TextureOrigin = new Vector2i(0, 46);
            State.Ramp = new Surface(1024, 1);
        }

        protected override void OnResize(ResizeEventArgs e)
        {
            base.OnResize(e);

            // update the opengl viewport
            GL.Viewport(0, 0, ClientSize.X, ClientSize.Y);

            // tell imgui of the new size
            _ui.OnResize(e, ClientSize);
        }

        protected override void OnLoad()
        {
            base.OnLoad();

            // initialise imgui
            _ui = new UI(Size);

            // initialise our drawing quad
            _quad = new Quad(new Surface(Size.X, Size.Y));
            _quad.OnLoad();
        }

        protected override void OnUnload()
        {
            base.OnUnload();

            _quad.OnUnload();

            // unbind all the resources by binding the targets to 0/null
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindVertexArray(0);
            GL.UseProgram(0);
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);

            Hotkeys.Update(KeyboardState);

            State State = State.GetState();

            // update imgui before we use its values
            _ui.OnUpdateFrame(this, e);

            // show some imgui elements
            // ImGuiNET.ImGui.ShowDemoWindow();

            // called once per frame; app logic
            var keyboard = KeyboardState;
            if (keyboard.IsKeyDown(Keys.Escape)) this.Close();

            if (State.Vertices.Count > 0)
            {
                State.NearestVertex = State.FindNearestVertex(State.MousePosition - State.TextureOrigin);
                State.NearestSegment = State.FindNearestPoint(State.MousePosition - State.TextureOrigin, ref State.NearestPoint);
            }

            if (Hotkeys.State == (int)Hotkeys.HotkeyState.C)
                State.SelectedVertex = -1;

            if (Hotkeys.IsDown(Keys.LeftControl, Keys.S))
            {
                State.SaveToFile();
            }

            // compute colors each frame
            foreach (Vertex v in State.Vertices)
                v.ComputeColor(State.Texture, 1.0f);

            if (State.Vertices.Count > 1)
            {
                // compute ramp each frame
                List<Sample> samples = State.Samples;
                samples.Clear();

                float totalLength = State.TotalLengthOfGraph();
                float currentLength = 0.0f;
                int currentPixel = 0;

                // phase 1: determine locations in the ramp
                for (int j = 0; j < State.Vertices.Count - 1; j++)
                {
                    Vertex v1 = State.Vertices[j];
                    samples.Add(new Sample()
                    {
                        Size = v1.size,
                        PositionOnTexture = v1.coordinates,
                        IndexOnRamp = currentPixel,
                        Color = v1.color
                    });

                    // compute the distance to the next entry
                    Vertex v2 = State.Vertices[j + 1];
                    currentLength += (v1.coordinates - v2.coordinates).EuclideanLength;

                    // if we skip find the first one we don't skip
                    bool skipped = v2.skip;
                    while (v2.skip)
                    {
                        j = j + 1;
                        v2 = State.Vertices[j + 1];
                    }

                    if (skipped)
                    {
                        Vertex lastSkippedVertex = State.Vertices[j];
                        currentLength += (lastSkippedVertex.coordinates - v2.coordinates).EuclideanLength;
                    }

                    currentPixel = (int)(1023 * currentLength / totalLength);
                }

                Vertex last = State.Vertices[State.Vertices.Count - 1];
                samples.Add(new Sample()
                {
                    Size = last.size,
                    PositionOnTexture = last.coordinates,
                    IndexOnRamp = 1023,
                    Color = last.color
                });

                int sampleIndex = 0;
                for (int j = 0; j < State.Ramp.width; j++)
                {
                    Sample s1 = samples[sampleIndex];
                    Sample s2 = samples[sampleIndex + 1];

                    float fraction = ((float)j - s1.IndexOnRamp) / (s2.IndexOnRamp - s1.IndexOnRamp);
                    State.Ramp.pixelsFloat[j] = (1 - fraction) * s1.Color + fraction * s2.Color;

                    if (s2.IndexOnRamp < j)
                        sampleIndex++;
                }
            }
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            // clear out previous content
            GL.ClearColor(new Color4(0, 32, 48, 255));
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);

            // populate the screen
            this.OnRender(_quad.screen, e);

            // render the screen on the quad
            _quad.OnRender();

            // draw imgui last to make sure it is on top
            _ui.OnRenderFrame(e);

            // check for any errors, specifically for imgui errors
            ImGuiUtilities.CheckGLError("End of frame");

            // show the frame to the user
            SwapBuffers();
        }

        public void OnRender(Surface screen, FrameEventArgs e)
        {
            screen.Clear(Vector4.Zero);

            State State = State.GetState();

            // draw out the texture
            for (int y = 0; y < State.Texture.height; y++)
            {
                int ly = y + State.TextureOrigin.Y;
                if (ly >= 0 && ly < screen.height)
                {
                    for (int x = 0; x < State.Texture.width; x++)
                    {
                        int lx = x + State.TextureOrigin.X;
                        if (lx >= 0 && lx < screen.width)
                        {
                            int indexScreen = lx + ly * screen.width;
                            int indexTexture = x + y * State.Texture.width;
                            screen.pixelsFloat[indexScreen] = State.Texture.pixelsFloat[indexTexture];
                        }
                    }
                }
            }

            // draw out the ramp
            int rampStart = 25;
            int rampOffset = 10;

            screen.Box(
                State.RampOffset.X - 1, State.RampOffset.Y - 1,
                State.RampOffset.X + Math.Min(screen.width, State.Ramp.width), State.RampOffset.Y + State.RampHeight,
                0.5f * Vector4.One
                );

            for (int y = rampStart; y < rampStart + State.RampHeight; y++)
            {
                if (y >= 0 && y < screen.height)
                {
                    for (int x = 0; x < State.Ramp.width; x++)
                    {
                        int sx = x + rampOffset;
                        if (sx >= 0 && sx < screen.width)
                        {
                            int indexScreen = sx + y * screen.width;
                            int indexTexture = x;
                            screen.pixelsFloat[indexScreen] = State.Ramp.pixelsFloat[indexTexture];
                        }
                    }
                }
            }

            // draw out vertex information
            if (State.Vertices.Count > 0)
            {
                // draw out the vertices
                if (Hotkeys.IsUp(Keys.Q))
                    foreach (Vertex v in State.Vertices)
                        v.OnRender(screen, State.TextureOrigin, Vector4.One);

                // draw out lines between vertices
                if (Hotkeys.IsUp(Keys.A))
                {
                    for (int j = 0; j < State.Vertices.Count - 1; j++)
                    {
                        Vertex v1 = State.Vertices[j];
                        Vertex v2 = State.Vertices[j + 1];

                        float multiplier = 1.0f;
                        if ((v1.skip && v2.skip))
                            multiplier = 0.25f;

                        screen.Line(
                            v1.coordinates.X + State.TextureOrigin.X,
                            v1.coordinates.Y + State.TextureOrigin.Y,
                            v2.coordinates.X + State.TextureOrigin.X,
                            v2.coordinates.Y + State.TextureOrigin.Y,
                            multiplier * new Vector4(0.8f, 0.8f, 0.8f, 1.0f)
                            );
                    }
                }

                // draw out selected vertex
                if (State.SelectedVertex >= 0)
                {
                    State.Vertices[State.SelectedVertex].OnRender(screen, State.TextureOrigin, new Vector4(0.2f, 0.2f, 1.0f, 1.0f));

                    if (Hotkeys.IsDown(Keys.D))
                    {
                        State.Vertices.RemoveAt(State.SelectedVertex);
                        State.SelectedVertex = -1;
                    }
                }

                // draw out closest point
                if (State.SelectedVertex < 0)
                {
                    if (Hotkeys.IsDown(Keys.LeftAlt, Keys.LeftControl))
                    {
                        Vector2i mp = State.MousePosition;
                        Vector2i np = State.NearestPoint;

                        screen.Line(
                            mp.X,
                            mp.Y,
                            np.X + State.TextureOrigin.X,
                            np.Y + State.TextureOrigin.Y,
                            new Vector4(0.8f, 1.0f, 0.8f, 1.0f));
                    }
                }

                // Add bits to the ramp
                foreach(Sample sample in State.Samples)
                    screen.Bar(rampOffset + sample.IndexOnRamp - 1, rampStart + State.RampHeight - 2, rampOffset + sample.IndexOnRamp + 1, rampStart + State.RampHeight + 2, Vector4.One);
                
                // Highligh selected bit
                if (State.SelectedVertex >= 0)
                {
                    Sample sample = State.Samples[State.SelectedVertex];
                    screen.Bar(rampOffset + sample.IndexOnRamp - 1, rampStart + State.RampHeight - 2, rampOffset + sample.IndexOnRamp + 1, rampStart + State.RampHeight + 2, new Vector4(0.0f, 0.0f, 1.0f, 1.0f));
                }
            }
        }

        protected override void OnTextInput(TextInputEventArgs e)
        {
            State State = State.GetState();

            // if you use the input, don't pass it to imgui

            // make imgui aware of the input
            _ui.OnTextInput(e);
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            // if we're on top of Imgui, ignore
            if (_ui.IsHovered()) return;

            State State = State.GetState();

            if (e.Button == MouseButton.Left)
            {

                // allows us to click things
                State.MouseMoved = false;

                // check whether we want to add a vertex
                if (Hotkeys.IsDown(Keys.LeftControl, Keys.LeftAlt))
                {
                    Vertex v = new Vertex(State.MousePosition.X - State.TextureOrigin.X, State.MousePosition.Y - State.TextureOrigin.Y, 5);
                    State.Vertices.Insert(State.NearestSegment + 1, v);
                }

                if (Hotkeys.IsDown(Keys.LeftControl))
                {
                    // Add to the end of the graph
                    Vertex v = new Vertex(State.MousePosition.X - State.TextureOrigin.X, State.MousePosition.Y - State.TextureOrigin.Y, 5);
                    State.Vertices.Add(v);
                }
            }
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            // if we're on top of Imgui, ignore
            if (_ui.IsHovered()) return;

            State State = State.GetState();

            if (e.Button == MouseButton.Left)
            { 
                if (!State.MouseMoved)
                {
                    if (State.Vertices.Count > 0)
                    {
                        if (Hotkeys.IsUp(Keys.LeftControl))
                        {
                            Vertex nearest = State.Vertices[State.NearestVertex];
                            State.SelectedVertex = -1;
                            if ((nearest.coordinates - (State.MousePosition - State.TextureOrigin)).EuclideanLength < Math.Max(nearest.size, 5.0))
                            {
                                State.SelectedVertex = State.NearestVertex;
                            }
                        }
                    }
                }
            }

            State.MouseMoved = false;
        }

        protected override void OnMouseMove(MouseMoveEventArgs e)
        {
            // if we're on top of Imgui, ignore
            if (_ui.IsHovered()) return;

            State State = State.GetState();

            // update the mouse position
            State.MousePosition = new Vector2i((int)e.X, (int)e.Y);

            // do not move if we intent to place points
            if (Hotkeys.IsUp(Keys.LeftControl))
            {
                if (Hotkeys.State == (int)Hotkeys.HotkeyState.ALT)
                {
                    if (State.SelectedVertex >= 0)
                    {
                        Vertex v = State.Vertices[State.SelectedVertex];
                        v.coordinates.X += (int)e.DeltaX;
                        v.coordinates.Y += (int)e.DeltaY;
                    }
                }

                if (MouseState.IsButtonDown(MouseButton.Left))
                {
                    State.MouseMoved = true;
                    State.TextureOrigin += new Vector2i((int)e.DeltaX, (int)e.DeltaY);
                }
            }
        }

        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            base.OnMouseWheel(e);

            State State = State.GetState();

            // make imgui aware of the input
            _ui.OnMouseWheel(e);
        }
    }
}
